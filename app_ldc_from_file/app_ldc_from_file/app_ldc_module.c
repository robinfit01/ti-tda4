/*
 *
 * Copyright (c) 2020 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "app_ldc_module.h"
#include "ldc_lut_1280x720.h"

static uint8_t  g_ldc_lut[] = LDC_LUT_1280_720;

vx_status app_init_ldc(vx_context context, LDCObj *ldcObj,  char *objName)
{
    vx_status status = VX_SUCCESS;

    vx_uint32 table_width_ds, table_height_ds;
    vx_imagepatch_addressing_t image_addr;
    vx_rectangle_t rect;

    ldcObj->table_width  = LDC_TABLE_WIDTH;
    ldcObj->table_height = LDC_TABLE_HEIGHT;
    ldcObj->ds_factor    = LDC_DS_FACTOR;


    ldcObj->dcc_config = NULL;

    table_width_ds = (((ldcObj->table_width / (1 << ldcObj->ds_factor)) + 1u) + 15u) & (~15u);
    table_height_ds = ((ldcObj->table_height / (1 << ldcObj->ds_factor)) + 1u);

    /* Mesh Image */
    ldcObj->mesh_img = vxCreateImage(context, table_width_ds, table_height_ds, VX_DF_IMAGE_U32);

    /* Copy Mesh table */
    rect.start_x = 0;
    rect.start_y = 0;
    rect.end_x = table_width_ds;
    rect.end_y = table_height_ds;

    image_addr.dim_x = table_width_ds;
    image_addr.dim_y = table_height_ds;
    image_addr.stride_x = 4u;
    image_addr.stride_y = table_width_ds * 4u;

    status = vxCopyImagePatch(ldcObj->mesh_img,
                              &rect, 0,
                              &image_addr,
                              g_ldc_lut,
                              VX_WRITE_ONLY,
                              VX_MEMORY_TYPE_HOST);
    if (VX_SUCCESS != status)
    {
        APP_PRINTF("Copy Image Failed\n");
        return VX_FAILURE;
    }

    /* Mesh Parameters */
    memset(&ldcObj->mesh_params, 0, sizeof(tivx_vpac_ldc_mesh_params_t));
    tivx_vpac_ldc_mesh_params_init(&ldcObj->mesh_params);
    ldcObj->mesh_params.mesh_frame_width  = ldcObj->table_width;
    ldcObj->mesh_params.mesh_frame_height = ldcObj->table_height;
    ldcObj->mesh_params.subsample_factor  = ldcObj->ds_factor;

    ldcObj->mesh_config = vxCreateUserDataObject(context, "tivx_vpac_ldc_mesh_params_t", sizeof(tivx_vpac_ldc_mesh_params_t), NULL);
    vxCopyUserDataObject(ldcObj->mesh_config, 0,
                         sizeof(tivx_vpac_ldc_mesh_params_t),
                         &ldcObj->mesh_params,
                         VX_WRITE_ONLY,
                         VX_MEMORY_TYPE_HOST);

    /* Block Size parameters */
    ldcObj->region_params.out_block_width  = LDC_BLOCK_WIDTH;
    ldcObj->region_params.out_block_height = LDC_BLOCK_HEIGHT;
    ldcObj->region_params.pixel_pad        = LDC_PIXEL_PAD;

    ldcObj->region_config = vxCreateUserDataObject(context, "tivx_vpac_ldc_region_params_t", sizeof(tivx_vpac_ldc_region_params_t),  NULL);
    vxCopyUserDataObject(ldcObj->region_config, 0,
                         sizeof(tivx_vpac_ldc_region_params_t),
                         &ldcObj->region_params,
                         VX_WRITE_ONLY,
                         VX_MEMORY_TYPE_HOST);

    /* LDC Configuration */
    tivx_vpac_ldc_params_init(&ldcObj->params);
    ldcObj->params.luma_interpolation_type = 1;
    //ldcObj->params.dcc_camera_id = sensorObj->sensorParams.dccId;

    ldcObj->config = vxCreateUserDataObject(context, "tivx_vpac_ldc_params_t", sizeof(tivx_vpac_ldc_params_t), NULL);

    vxCopyUserDataObject(ldcObj->config, 0,
                         sizeof(tivx_vpac_ldc_params_t),
                         &ldcObj->params,
                         VX_WRITE_ONLY,
                         VX_MEMORY_TYPE_HOST);


    /* LDC Output image in NV12 format */
    vx_image output_img = vxCreateImage(context, ldcObj->table_width, ldcObj->table_height, VX_DF_IMAGE_NV12);

    ldcObj->output_arr = vxCreateObjectArray(context, (vx_reference)output_img, 4);

    vxReleaseImage(&output_img);

    ldcObj->file_path   = NULL;
    ldcObj->file_prefix = NULL;
    ldcObj->write_node  = NULL;
    ldcObj->write_cmd   = NULL;

    return (status);
}

void app_deinit_ldc(LDCObj *ldcObj)
{
    vxReleaseUserDataObject(&ldcObj->config);
    vxReleaseUserDataObject(&ldcObj->region_config);
    vxReleaseUserDataObject(&ldcObj->mesh_config);

    vxReleaseImage(&ldcObj->mesh_img);
    vxReleaseObjectArray(&ldcObj->output_arr);

    if(ldcObj->dcc_config != NULL)
    {
        vxReleaseUserDataObject(&ldcObj->dcc_config);
    }
    if(ldcObj->en_out_ldc_write == 1)
    {
        vxReleaseArray(&ldcObj->file_path);
        vxReleaseArray(&ldcObj->file_prefix);
        vxReleaseUserDataObject(&ldcObj->write_cmd);
    }
}

void app_delete_ldc(LDCObj *ldcObj)
{
    if(ldcObj->node != NULL)
    {
        vxReleaseNode(&ldcObj->node);
    }
    if(ldcObj->write_node != NULL)
    {
        vxReleaseNode(&ldcObj->write_node);
    }
}

vx_status app_create_graph_ldc(vx_graph graph, LDCObj *ldcObj, vx_image input_img)
{
    vx_status status = VX_SUCCESS;

    vx_image output_img = (vx_image)vxGetObjectArrayItem(ldcObj->output_arr, 0);

    ldcObj->node = tivxVpacLdcNode(graph, ldcObj->config, NULL,
                                ldcObj->region_config, ldcObj->mesh_config,
                                ldcObj->mesh_img, NULL, input_img,
                                output_img, NULL);
    vxReleaseImage(&output_img);
    return status;
}




