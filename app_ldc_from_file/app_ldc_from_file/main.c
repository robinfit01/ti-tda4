

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <assert.h>

#include <VX/vx.h>
#include <VX/vxu.h>
#include <VX/vx_khr_pipelining.h>
#include "math.h"
#include "tivx_utils_file_rd_wr.h"
#include <limits.h>
#include <TI/tivx_srv.h>

#include <TI/tivx_task.h>
#include <tivx_utils_graph_perf.h>
#include <utils/perf_stats/include/app_perf_stats.h>
#ifdef J7
#include <app_init.h>
#endif
#include "app_common.h"
#include "app_display_module.h"

#include "ldc_lut_1280x720.h"
#include "app_ldc_module.h"



typedef struct {
    /* OpenVX Types */
    vx_context context;
    vx_graph graph;

//disp object
    LDCObj ldcObj;
    DisplayObj dispObj;

    uint32_t is_interactive;
    uint32_t test_mode;
    tivx_task task;

    vx_image camera_output_image;

    uint32_t stop_task;
    uint32_t stop_task_done;

} AppObj;

AppObj gAppObj;

static void app_parse_cmd_line_args(AppObj *obj, int argc, char *argv[]);
static void app_init(AppObj *obj);
static void app_deinit(AppObj *obj);
static vx_status app_create_graph(AppObj *obj);

static void app_delete_graph(AppObj *obj);
vx_status app_load_image_form_uyvy(char *file_name, vx_image *in_img);

//显示教程
static void app_show_usage(int argc, char* argv[])
{
    printf("\n");
    printf(" SRV Demo - (c) Texas Instruments 2019\n");
    printf(" ========================================================\n");
    printf("\n");
    printf(" Usage,\n");
    printf("  %s --cfg <config file>\n", argv[0]);
    printf("\n");
}

//设置配置初始化
static void app_set_cfg_default(AppObj *obj)
{
    obj->is_interactive = 0;
    obj->test_mode = 0;
}


//解析配置文件
static void app_parse_cfg_file(AppObj *obj, char *cfg_file_name)
{
    FILE *fp = fopen(cfg_file_name, "r");
    char line_str[1024];
    char *token;

    if(fp==NULL)
    {
        printf("# ERROR: Unable to open config file [%s]\n", cfg_file_name);
        exit(0);
    }

    while(fgets(line_str, sizeof(line_str), fp)!=NULL)
    {
        char s[]=" \t";

        if (strchr(line_str, '#'))
        {
            continue;
        }

        /* get the first token */
        token = strtok(line_str, s);

        if (NULL != token)
        {
            if(strcmp(token, "is_interactive")==0)
            {
                token = strtok(NULL, s);
                if (NULL != token)
                {
                    token[strlen(token)-1]=0;
                    obj->is_interactive = atoi(token);
                    if(obj->is_interactive > 1)
                        obj->is_interactive = 1;
                }
            }
        }
    }

    fclose(fp);
}

//解析命令行参数
static void app_parse_cmd_line_args(AppObj *obj, int argc, char *argv[])
{
    int i;
    vx_bool set_test_mode = vx_false_e;

    app_set_cfg_default(obj);

    if(argc==1)
    {
        app_show_usage(argc, argv);
        exit(0);
    }

    for(i=0; i<argc; i++)
    {
        if(strcmp(argv[i], "--cfg")==0)
        {
            i++;
            if(i>=argc)
            {
                app_show_usage(argc, argv);
            }
            app_parse_cfg_file(obj, argv[i]);
        }
    }
    if(set_test_mode == vx_true_e)
    {
        obj->test_mode = 1;
        obj->is_interactive = 0;
    }
}

static void app_init(AppObj *obj)
{
#ifdef J7
    appCommonInit();
#endif
    tivxInit();
    tivxHostInit();

    obj->context = vxCreateContext();
    APP_ASSERT_VALID_REF(obj->context);


    tivxHwaLoadKernels(obj->context);       //tiovx加载硬件驱动kernels
    obj->dispObj.display_option = 1;
    app_init_display(obj->context, &obj->dispObj, "display_obj");       //初始化显示
    app_init_ldc(obj->context,&obj->ldcObj,"myLdcObj");                 //初始化LDC模块
}

static int app_create_graph(AppObj *obj)
{
    vx_uint32 in_width, in_height;
    vx_status status = VX_SUCCESS;

    obj->graph = vxCreateGraph(obj->context);

    //图像为原始图像 1280 × 944 的鱼眼摄像头的原始图像，鱼眼摄像头，给定的图像为 UYVY 格式的图像
    in_width = 1280;        //输入图像的宽度
    in_height = 944;        //输入图像的高度

    //创建相机输出图像，1280 × 944 UYVY 格式
    obj->camera_output_image = vxCreateImage(obj->context, in_width, in_height, VX_DF_IMAGE_UYVY);

    if(status == VX_SUCCESS)
    {   //获取图像创建的状态
        status = vxGetStatus((vx_reference) (obj->camera_output_image));    
    }

    if (status == VX_SUCCESS)
    {   //使用相机输出的图像，作为LDC 畸变校正模块的输入
        status = app_create_graph_ldc(obj->graph, &obj->ldcObj, obj->camera_output_image);
    }

    if (status == VX_SUCCESS)
    {
        //因为LDC模块输出的是以一个array的形式，所以要从array里面取出这个转换以后的图像
        vx_image image = (vx_image)vxGetObjectArrayItem(obj->ldcObj.output_arr, 0);

        //将从LDC模块取出的经过畸变校正的图像作为显示模块的输入
        status = app_create_graph_display(obj->graph, &obj->dispObj, image);

        //释放临时文件
        vxReleaseImage(&image);
    }

    if (status == VX_SUCCESS)
    {//校验Graph
        status = vxVerifyGraph(obj->graph);
        APP_PRINTF("app_srv_fileio: Verifying disp_graph... Done\n");
    }
    return status;
}

static void app_delete_graph(AppObj *obj)
{
    /* Deleting applib */
    vxReleaseImage(&obj->camera_output_image);
    vxReleaseNode(&obj->dispObj.disp_node);
    vxReleaseGraph(&obj->graph);
    return;
}

static void app_deinit(AppObj *obj)
{
    app_deinit_display(&obj->dispObj);
    tivxHwaUnLoadKernels(obj->context); // tiovx加载硬件驱动kernels
    vxReleaseContext(&obj->context);
    tivxHostDeInit();
    tivxDeInit();
#ifdef J7
    appCommonDeInit();
#endif
}

static void app_run_task(void *app_var)
{
    AppObj *obj = (AppObj *)app_var;
    u_char ch;
    while (obj->stop_task)
    {
        ch = getchar();
        switch(ch)
        {
            case 'x':
            {
                obj->stop_task = 0;
            }break;
            default:            break;
        }
    }
}

static vx_status app_run_task_create(AppObj * obj)
{
    tivx_task_create_params_t params;
    vx_status status;

    tivxTaskSetDefaultCreateParams(&params);
    params.task_main = app_run_task;
    params.app_var = obj;
    status = tivxTaskCreate(&obj->task, &params);
    return status;
}

int main(int argc, char* argv[])
{
    AppObj *obj = &gAppObj;
    vx_status status = VX_SUCCESS;

    app_parse_cmd_line_args(obj, argc, argv);
    app_init(obj);
    status = app_create_graph(obj);

    obj->stop_task = 1;
    app_run_task_create(obj);

    if (VX_SUCCESS == status)
    {
        while (obj->stop_task)
        {
            //从文件中读取 UYVY 格式的图像，作为LDC模块的输入图像
            app_load_image_form_uyvy("./ldc_1280_720_uyvy.yuv" , &obj->camera_output_image);
            if (status == VX_SUCCESS)
            {
                //执行Graph，运行LDC 畸变校正，然后显示图
                status = vxProcessGraph(obj->graph);
            }
            printf("LDC Demo running !!! \n");
            
            tivxTaskWaitMsecs(100);
        }
        app_delete_graph(obj);
        app_deinit(obj);
    }
    return status;
}

//从UYVY文件中读取图像
vx_status app_load_image_form_uyvy(char *file_name , vx_image *in_img)
{
    vx_status status;
    FILE *fp = fopen(file_name, "rb");

    if (fp == NULL)
    {
        printf("Unable to open file %s \n", file_name);
        return (VX_FAILURE);
    }

    vx_rectangle_t rect;
    vx_imagepatch_addressing_t image_addr;
    vx_map_id map_id;
    void *data_ptr;
    vx_uint32 img_width;
    vx_uint32 img_height;
    vx_uint32 img_format;

    vx_uint32 num_bytes;

    vxQueryImage(*in_img, VX_IMAGE_WIDTH, &img_width, sizeof(vx_uint32));
    vxQueryImage(*in_img, VX_IMAGE_HEIGHT, &img_height, sizeof(vx_uint32));
    vxQueryImage(*in_img, VX_IMAGE_FORMAT, &img_format, sizeof(vx_uint32));

    rect.start_x = 0;
    rect.start_y = 0;
    rect.end_x = img_width;
    rect.end_y = img_height;

    //将等待输入的图像映射到用户态
    status = vxMapImagePatch(*in_img,
                                &rect,
                                0,
                                &map_id,
                                &image_addr,
                                &data_ptr,
                                VX_WRITE_ONLY,
                                VX_MEMORY_TYPE_HOST,
                                VX_NOGAP_X);

    // Copy Luma
    //拷贝文件里面的所有数据到目标图像 UYVY 格式图像，大小是 img_width * img_height * 2
    num_bytes = fread(data_ptr, 2, img_width * img_height, fp);

    if (num_bytes != (img_width * img_height))
        printf("Luma bytes read = %d, expected = %d\n", num_bytes, img_width * img_height);

    vxUnmapImagePatch(*in_img, map_id);

    fclose(fp);
    return (status);
}
