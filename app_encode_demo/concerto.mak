ifeq ($(TARGET_CPU),$(filter $(TARGET_CPU), x86_64 A72))

include $(PRELUDE)

TARGET      := vx_app_encode_decode_demo

CSOURCES    := imagenet_class_labels.c main.c

ifeq ($(HOST_COMPILER),GCC_LINUX)
CFLAGS += -Wno-unused-function
endif


ifeq ($(TARGET_CPU),x86_64)

TARGETTYPE  := exe

CSOURCES    += main_x86.c

include $(VISION_APPS_PATH)/apps/concerto_x86_64_inc.mak

endif

ifeq ($(TARGET_CPU),A72)
ifeq ($(TARGET_OS),$(filter $(TARGET_OS), LINUX QNX))

TARGETTYPE  := exe

CSOURCES    += main_linux_arm.c
CSOURCES    += app_usb_camera.c 

CSOURCES    += app_encode_module.c
CSOURCES    += app_encode_module_mutil.c
CSOURCES    += app_decode_module.c
CSOURCES    += app_display_module.c


include $(VISION_APPS_PATH)/apps/concerto_a72_inc.mak

ifeq ($(TARGET_OS),LINUX)
CFLAGS += -DEGL_NO_X11
SYS_SHARED_LIBS += gbm
endif


endif
endif

ifeq ($(TARGET_OS),SYSBIOS)
ifeq ($(TARGET_CPU),$(filter $(TARGET_CPU), A72))
TARGETTYPE  := library
endif
endif

SYS_SHARED_LIBS += EGL
SYS_SHARED_LIBS += GLESv2

IDIRS += $(IMAGING_IDIRS)
IDIRS += $(VISION_APPS_SRV_IDIRS)
IDIRS += $(VISION_APPS_KERNELS_IDIRS)

STATIC_LIBS += $(IMAGING_LIBS)
STATIC_LIBS += $(VISION_APPS_SRV_LIBS)
STATIC_LIBS += $(TIADALG_LIBS)
STATIC_LIBS += $(VISION_APPS_KERNELS_LIBS)

include $(FINALE)

endif
