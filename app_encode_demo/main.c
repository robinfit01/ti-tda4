/*
 *
 * Copyright (c) 2017 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <utils/draw2d/include/draw2d.h>
#include <utils/perf_stats/include/app_perf_stats.h>
#include "itidl_ti.h"
#include "app_common.h"
#include "app_usb_camera.h"
#include "app_encode_module.h"
#include "app_encode_module_mutil.h"
#include "app_decode_module.h"
#include "app_display_module.h"


/*
 * This is the size of trace buffer allocated in host memory and
 * shared with target.
 */
#ifdef APP_DEBUG
#define APP_PRINTF(f_, ...) printf((f_), ##__VA_ARGS__)
#else
#define APP_PRINTF(f_, ...)
#endif

#define APP_BUFFER_Q_DEPTH (2)
#define APP_PIPELINE_DEPTH (6)

typedef struct {
    DisplayObj displayObj;          //显示相关的节点对象结构定义
    EncodeObj encodeObj;
    EncodeObjMutil encodeObjMutil;  //多路编码节点
    DecodeObj decodeObj;

    USBCameraObj usbCameraObj; //USB摄像头相关结构

    /* config options */
    
    char input_file_list[APP_MAX_FILE_PATH];


    /* OpenVX references */
    vx_context context;
    vx_graph graph;

    vx_uint32 delay_in_msecs;
    vx_uint32 num_iterations;

    uint32_t is_interactive;        //控制是否启动控制台
    vx_int32 test_mode;

    tivx_task task;
    uint32_t stop_task;
    uint32_t stop_task_done;

    app_perf_point_t total_perf;
    app_perf_point_t fileio_perf;
    app_perf_point_t draw_perf;

    FILE *fp;
    char *image;
    unsigned char num;

} AppObj;

AppObj gAppObj;

static int app_parse_cmd_line_args(AppObj *obj, int argc, char *argv[]);
static vx_status app_init(AppObj *obj);
static void app_deinit(AppObj *obj);
static vx_status app_create_graph(AppObj *obj);
static vx_status app_verify_graph(AppObj *obj);
static vx_status app_run_graph(AppObj *obj);
static vx_status app_run_graph_interactive(AppObj *obj);
static void app_delete_graph(AppObj *obj);


#ifdef APP_WRITE_PRE_PROC_OUTPUT
static vx_status writePreProcOutput(char* file_name, vx_tensor output);
#endif



int app_tidl_main(int argc, char* argv[])
{
    vx_status status = VX_SUCCESS;

    AppObj *obj = &gAppObj;

    status = app_parse_cmd_line_args(obj, argc, argv);
    if(status == VX_SUCCESS)
    {
        status = app_init(obj);         //初始化操作，用于注册相关kernel 用于初始化参数等
    }
    if(status == VX_SUCCESS)
    {
        status = app_create_graph(obj); //创建Graph
    }
    if(status == VX_SUCCESS)
    {
        status = app_verify_graph(obj); //
    }
    if(status == VX_SUCCESS)
    {
        if(obj->is_interactive)
        {
            status = app_run_graph_interactive(obj);        //执行带有交互操作的程序
        }
        else
        {
            status = app_run_graph(obj);                    //无交互
            if(status == VX_FAILURE)
            {
                printf("Error processing graph!\n");
            }
        }
    }
    app_delete_graph(obj);
    app_deinit(obj);
    return status;
}

static int app_init(AppObj *obj)
{

    vx_status status = VX_SUCCESS;

    APP_PRINTF("app_tidl: Init ... \n");

    obj->context = vxCreateContext();
    APP_ASSERT_VALID_REF(obj->context);


    tivxHwaLoadKernels(obj->context);

#ifndef ENCODE_DECODE_1080P_ENABLE        //如果没有使能1080P 测试，则进行USB 摄像头获取YUV 720P shu

    //USB 摄像头相关初始化
    if (status == VX_SUCCESS)
    {
        obj->usbCameraObj.devName = malloc(64);                          //申请内存
        memset(obj->usbCameraObj.devName, 0, 64);                        //内存清空
        sprintf(obj->usbCameraObj.devName, "/dev/video%d",0);        //导入摄像的硬件名，需要根据系统的实际设备名变更

        if (status == VX_SUCCESS)
        {
            status = app_init_usbCamera(&obj->context,&(obj->usbCameraObj)); //初始化usb摄像头
        }
        else
        {
            printf("USB Camera Running error!\n");
        }
    }
#else

#endif


//显示部分初始化操作
    if (status == VX_SUCCESS)
    {
        status = app_init_display(obj->context, &obj->displayObj, "display_obj"); //初始化显示部分的内容
    }

    if (status == VX_SUCCESS)
    {

#ifndef ENCODE_MUTIL_ENABLE     //单路编码
        //必要的初始化配置相关，设置输出文件路径
        sprintf(obj->encodeObj.output_file_path, "/home/root");
        
        obj->encodeObj.inHeight = obj->usbCameraObj.fmt.fmt.pix.height;
        obj->encodeObj.inWidth = obj->usbCameraObj.fmt.fmt.pix.width;

        status = app_init_encode(obj->context, &obj->encodeObj);
        APP_PRINTF("Encode init done!\n");
#else   //多路编码

    #ifndef ENCODE_DECODE_1080P_ENABLE
            obj->encodeObjMutil.inHeight = obj->usbCameraObj.fmt.fmt.pix.height;
            obj->encodeObjMutil.inWidth = obj->usbCameraObj.fmt.fmt.pix.width;
    #else
            obj->encodeObjMutil.inHeight = 1088;
            obj->encodeObjMutil.inWidth = 1920;
    #endif

        status = app_init_encode_mutil(obj->context, &obj->encodeObjMutil);
        APP_PRINTF("EncodeMutil init done!\n");
#endif
    }

    if(status == VX_SUCCESS)
    {
#ifndef ENCODE_MUTIL_ENABLE     //单路编码
        obj->decodeObj.height = 768;        //decode节点运行时，需要设置图像的高度为64的整数倍。在正常读取的时候，读取前面720行即可。
        obj->decodeObj.width = 1280;

        app_init_decode(obj->context, &obj->decodeObj);
#else                           //多路编码

#endif
    }

#ifndef ENCODE_DECODE_1080P_ENABLE

    //创建一个和摄像头输出大小相同的图
    obj->displayObj.disp_image = vxCreateImage(obj->context, IMAGEWIDTH, IMAGEHEIGHT, VX_DF_IMAGE_NV12);
    APP_ASSERT_VALID_REF(obj->displayObj.disp_image);

#else
    //创建一个1080P 图像
    obj->displayObj.disp_image = vxCreateImage(obj->context, 1920, 1080, VX_DF_IMAGE_NV12);
    APP_ASSERT_VALID_REF(obj->displayObj.disp_image);

#endif


    APP_PRINTF("app_templete: Init ... Done.\n");

    return status;
}

static void app_deinit(AppObj *obj)
{
    APP_PRINTF("app_templete: De-init ... \n");

    tivxTIDLUnLoadKernels(obj->context);

    if (vx_true_e == tivxIsTargetEnabled(TIVX_TARGET_DISPLAY1) && (obj->displayObj.display_option == 1))
    {
        tivxHwaUnLoadKernels(obj->context);
    }
    vxReleaseContext(&obj->context);

    APP_PRINTF("app_templete: De-init ... Done.\n");
}

static void app_delete_graph(AppObj *obj)
{
    APP_PRINTF("app_tidl: Delete ... \n");

    if (vx_true_e == tivxIsTargetEnabled(TIVX_TARGET_DISPLAY1) && (obj->displayObj.display_option == 1))
    {
        vxReleaseNode(&obj->displayObj.disp_node);
        vxReleaseGraph(&obj->displayObj.disp_graph);
    }
    
    vxReleaseGraph(&obj->displayObj.disp_graph);

    if ((vx_true_e == tivxIsTargetEnabled(TIVX_TARGET_DISPLAY1)) && (obj->displayObj.display_option == 1))
    {
        vxReleaseImage(&obj->displayObj.disp_image);
        vxReleaseUserDataObject(&obj->displayObj.disp_params_obj);
    }

    APP_PRINTF("app_tidl: Delete ... Done.\n");
}

static void app_show_usage(int argc, char* argv[])
{
    printf("\n");
    printf(" TIDL Demo - (c) Texas Instruments 2018\n");
    printf(" ========================================================\n");
    printf("\n");
    printf(" Usage,\n");
    printf("  %s --cfg <config file>\n", argv[0]);
    printf("\n");
}

static void app_set_cfg_default(AppObj *obj)
{
    snprintf(obj->input_file_list,APP_MAX_FILE_PATH, "test_data/psdkra/app_tidl/names.txt");

    obj->displayObj.display_option = 1;
    obj->delay_in_msecs = 0;
    obj->num_iterations = 1;
    obj->is_interactive = 0;
    obj->test_mode      = 0;
}

static int app_parse_cfg_file(AppObj *obj, char *cfg_file_name)
{
    FILE *fp = fopen(cfg_file_name, "r");
    char line_str[1024];
    char *token;

    if(fp==NULL)
    {
        printf("app_tidl: ERROR: Unable to open config file [%s]\n", cfg_file_name);
        return -1;
    }

    while(fgets(line_str, sizeof(line_str), fp)!=NULL)
    {
        char s[]=" \t";

        if (strchr(line_str, '#'))
        {
            continue;
        }

        /* get the first token */
        token = strtok(line_str, s);

        if(token != NULL)
        {
            if(strcmp(token, "input_file_list")==0)
            {
                token = strtok(NULL, s);
                if(token != NULL)
                {
                  token[strlen(token)-1]=0;
                  strcpy(obj->input_file_list, token);
                }
            }
            else
            if(strcmp(token, "display_option")==0)
            {
                token = strtok(NULL, s);
                if(token != NULL)
                {
                  token[strlen(token)-1]=0;
                  obj->displayObj.display_option = atoi(token);
                  if (obj->displayObj.display_option > 1)
                      obj->displayObj.display_option = 1;
                }
            }
            else
            if(strcmp(token, "delay")==0)
            {
                token = strtok(NULL, s);
                if(token != NULL)
                {
                  token[strlen(token)-1]=0;
                  obj->delay_in_msecs = atoi(token);
                  if(obj->delay_in_msecs > 2000)
                      obj->delay_in_msecs = 2000;
                }
            }
            else
            if(strcmp(token, "num_iterations")==0)
            {
                token = strtok(NULL, s);
                if(token != NULL)
                {
                  token[strlen(token)-1]=0;
                  obj->num_iterations = atoi(token);
                  if(obj->num_iterations == 0)
                      obj->num_iterations = 1;
                }
            }
            else
            if(strcmp(token, "is_interactive")==0)
            {
                token = strtok(NULL, s);
                if(token != NULL)
                {
                  token[strlen(token)-1]=0;
                  obj->is_interactive = atoi(token);
                  if(obj->is_interactive > 1)
                      obj->is_interactive = 1;
                }
            }
            else
            if(strcmp(token, "test_mode")==0)
            {
                token = strtok(NULL, s);
                if(token != NULL)
                {
                  token[strlen(token)-1]=0;
                  obj->test_mode = atoi(token);
                }
            }
        }
        if (obj->test_mode == 1)
        {
          obj->is_interactive = 0;
          /* display_option must be set to 1 in order for the checksums
              to come out correctly */
          obj->displayObj.display_option = 1;
        }
    }

    fclose(fp);

    return 0;
}

static int app_parse_cmd_line_args(AppObj *obj, int argc, char *argv[])
{
    int i;
    vx_bool set_test_mode = vx_false_e;

    app_set_cfg_default(obj);

    if(argc==1)
    {
        app_show_usage(argc, argv);
        return -1;
    }

    for(i=0; i<argc; i++)
    {
        if(strcmp(argv[i], "--cfg")==0)
        {
            i++;
            if(i>=argc)
            {
                app_show_usage(argc, argv);
            }
            app_parse_cfg_file(obj, argv[i]);
        }
        else
        if(strcmp(argv[i], "--help")==0)
        {
            app_show_usage(argc, argv);
            return -1;
        }
        else
        if(strcmp(argv[i], "--test")==0)
        {
            set_test_mode = vx_true_e;
        }
    }

    if (set_test_mode == vx_true_e)
    {
        obj->test_mode = 1;
        obj->is_interactive = 0;
        obj->displayObj.display_option = 1;
        obj->delay_in_msecs = 100;
    }

    #ifdef x86_64
    obj->display_option = 0;
    obj->is_interactive = 0;
    #endif

    return 0;
}


static vx_status app_create_graph(AppObj *obj)
{
    vx_status status = VX_SUCCESS;

    APP_PRINTF("app_tidl: Creating graph ... \n");

    /* Create OpenVx App Graph */
    obj->graph = vxCreateGraph(obj->context);       //在上下文创建APP graph
    APP_ASSERT_VALID_REF(obj->graph);
    vxSetReferenceName((vx_reference)obj->graph, "AppGraph");

    /* Create OpenVx Graph */
    obj->displayObj.disp_graph = vxCreateGraph(obj->context);
    APP_ASSERT_VALID_REF(obj->displayObj.disp_graph)
    vxSetReferenceName((vx_reference)obj->displayObj.disp_graph, "Display");

    /* Create Decode Graph */
    obj->decodeObj.graph = vxCreateGraph(obj->context);
    APP_ASSERT_VALID_REF(obj->decodeObj.graph)
    vxSetReferenceName((vx_reference)obj->decodeObj.graph, "Decodegraph");

#ifndef ENCODE_DECODE_1080P_ENABLE

    if(status==VX_SUCCESS)
    {
        status = app_create_graph_USBColorConvert(obj->graph, &(obj->usbCameraObj));
    }
#else 


#endif

    if (status == VX_SUCCESS) //检测是否成功，然后创建encode节点
    {
#ifndef ENCODE_MUTIL_ENABLE
        status = app_create_graph_encode(obj->graph, &obj->encodeObj, &obj->usbCameraObj.imageOutputNV12);
#else   //创建多路编码

        #ifndef ENCODE_DECODE_1080P_ENABLE

        status = app_create_graph_encode_mutil(obj->graph, &obj->encodeObjMutil, &obj->usbCameraObj.imageOutputNV12);
        #else

        //测试1080P 图像，显示的图像创建的是1080P 的图像，所以使用这个作为encode节点的输入
        status = app_create_graph_encode_mutil(obj->graph, &obj->encodeObjMutil, &obj->encodeObjMutil.encode_image);

        #endif
#endif
    }

    if (status == VX_SUCCESS) //检测是否成功，然后创建decode节点
    {
#ifndef ENCODE_MUTIL_ENABLE
        status = app_create_graph_decode(obj->decodeObj.graph, &obj->decodeObj, obj->decodeObj.bitstream_obj);
#else

#endif
    }

    if (status == VX_SUCCESS)
    {
#ifndef ENCODE_DECODE_1080P_ENABLE
        //将USB 摄像头经过colorConverte后的NV12图像作为显示节点的输入
        status = app_create_graph_display(obj->displayObj.disp_graph, &obj->displayObj, obj->usbCameraObj.imageOutputNV12);
#else   //如果测试1080P视频，则显示的图像是来自SD 卡的文件
        status = app_create_graph_display(obj->displayObj.disp_graph, &obj->displayObj, obj->displayObj.disp_image);
#endif
   }

    if (vx_true_e == tivxIsTargetEnabled(TIVX_TARGET_DISPLAY1) && (obj->displayObj.display_option == 1))
    {
        vxSetReferenceName((vx_reference)obj->displayObj.disp_params_obj, "DisplayParams");
    }




    APP_PRINTF("app_tidl: Creating graph ... Done.\n");

    return status;
}

static void app_run_task(void *app_var)
{
    AppObj *obj = (AppObj *)app_var;
    vx_status status = VX_SUCCESS;
    appPerfStatsCpuLoadResetAll();

    while(!obj->stop_task)
    {
        status = app_run_graph(obj);
        if(status == VX_FAILURE)
        {
            printf("Error processing graph!\n");
            obj->stop_task = 1;
        }
    }
    obj->stop_task_done = 1;
}

static int32_t app_run_task_create(AppObj *obj)
{
    tivx_task_create_params_t params;
    int32_t status;

    tivxTaskSetDefaultCreateParams(&params);
    params.task_main = app_run_task;
    params.app_var = obj;

    obj->stop_task_done = 0;
    obj->stop_task = 0;

    status = tivxTaskCreate(&obj->task, &params);

#ifndef ENCODE_DECODE_1080P_ENABLE
    //如果没有使能1080P 则创建USB 摄像头任务，获取USB 摄像头图像 720P
    obj->usbCameraObj.run_task = &app_usb_camera_task_runVideo1;
    app_usb_camera_task_create(&obj->usbCameraObj);

#else

#endif


    return status;
}

static void app_run_task_delete(AppObj *obj)
{
    while(obj->stop_task_done==0)
    {
         tivxTaskWaitMsecs(100);
    }

    tivxTaskDelete(&obj->task);
}

static char menu[] = {
    "\n"
    "\n ================================="
    "\n Demo : TIDL Object Classification"
    "\n ================================="
    "\n"
    "\n p: Print performance statistics"
    "\n"
    "\n x: Exit"
    "\n"
    "\n Enter Choice: "
};

static vx_status app_run_graph_interactive(AppObj *obj)
{
    vx_status status = VX_SUCCESS;
    uint32_t done = 0;
    char ch;
    FILE *fp;
    app_perf_point_t *perf_arr[1];

    status = app_run_task_create(obj);
    if(status != VX_SUCCESS)
    {
        printf("app_tidl: ERROR: Unable to create task\n");
    }
    else
    {
        while((!done) && (status == VX_SUCCESS))
        {
            printf(menu);
            ch = getchar();
            printf("\n");

            switch(ch)
            {
                case 'p':
                    appPerfStatsPrintAll();
                    if(status == VX_SUCCESS)
                    {
                        status = tivx_utils_graph_perf_print(obj->displayObj.disp_graph);
                    }
                    if(status == VX_SUCCESS)
                    {
                        status = tivx_utils_graph_perf_print(obj->displayObj.disp_graph);
                    }
                    appPerfPointPrint(&obj->fileio_perf);
                    appPerfPointPrint(&obj->draw_perf);
                    appPerfPointPrint(&obj->total_perf);
                    printf("\n");
                    appPerfPointPrintFPS(&obj->total_perf);
                    printf("\n");
                    break;
                case 'e':
                    perf_arr[0] = &obj->total_perf;
                    fp = appPerfStatsExportOpenFile(".", "dl_demos_app_tidl");
                    if((NULL != fp) && (status == VX_SUCCESS))
                    {
                        appPerfStatsExportAll(fp, perf_arr, 1);
                        status = tivx_utils_graph_perf_export(fp, obj->displayObj.disp_graph);
                        appPerfStatsExportCloseFile(fp);
                        appPerfStatsResetAll();
                    }
                    else
                    {
                        printf("fp is null\n");
                    }
                    break;
                case 'x':
                    obj->stop_task = 1;
                    done = 1;
                    break;
            }
        }
        app_run_task_delete(obj);
    }
    return status;
}

static vx_status app_verify_graph(AppObj *obj)
{
    vx_status status = VX_SUCCESS;

    APP_PRINTF("app_tidl: Verifying graph ... \n");

    if (status == VX_SUCCESS)
    {
        status = vxVerifyGraph(obj->graph);
        if (status != VX_SUCCESS)
        {
            printf("app_tidl: ERROR: Verifying app graph ... Failed !!!\n");
            return status;
        }
        APP_PRINTF("app_tidl: Verifying app graph ... Done.\n");
    }

    if ((vx_true_e == tivxIsTargetEnabled(TIVX_TARGET_DISPLAY1)) && (obj->displayObj.display_option == 1))
    {
        APP_PRINTF("app_tidl: Verifying display graph ... \n");

        /* Verify the TIDL Graph */
        status = vxVerifyGraph(obj->displayObj.disp_graph);
        if (status != VX_SUCCESS)
        {
            printf("app_tidl: ERROR: Verifying display graph ... Failed !!!\n");
            return status;
        }
        APP_PRINTF("app_tidl: Verifying display graph ... Done.\n");
    }

#ifndef ENCODE_DECODE_1080P_ENABLE
    if(vx_true_e == tivxIsTargetEnabled(TIVX_TARGET_VDEC1))
    {
        APP_PRINTF("app_tidl: Verifying decode graph ... \n");
        status = vxVerifyGraph(obj->decodeObj.graph);
        if (status != VX_SUCCESS)
        {
            printf("app_tidl: ERROR: Verifying decode graph ... Failed !!!\n");
            return status;
        }
        APP_PRINTF("app_tidl: Verifying decode graph ... Done.\n");
    }
#endif

    APP_PRINTF("app_tidl: Verifying graph ... Done.\n");
    /* wait a while for prints to flush */
    tivxTaskWaitMsecs(100);

    return status;
}


static vx_status app_run_graph_for_one_frame(AppObj *obj, char *curFileName, vx_uint32 counter)
{
    vx_status status = VX_SUCCESS;

#ifndef ENCODE_DECODE_1080P_ENABLE

    //通过USB摄像头获取图像
    if (status == VX_SUCCESS)
    {
        status = tivxMutexLock(obj->usbCameraObj.mutex); //上锁，准备获取USB数据，检查USB 是否已经完成图像的获取。防止干扰。实现任务之间同步。
        if (status == VX_SUCCESS)
        {
            APP_PRINTF("APP USB Infor : main task the mutex lock ok!\n");

            status = tivxMutexUnlock(obj->usbCameraObj.mutex); //解锁
        }
        else
        {
            APP_PRINTF("APP USB Infor : main task the mutex is busy!\n");
        }
    }
#else

//从SD卡获取1080P 图像

//将encode_image图像映射到内存，并等待操作
        vx_uint32 width, height;
        vx_imagepatch_addressing_t image_addr;
        vx_rectangle_t rect;
        vx_map_id map_id, map_id1;
        void *data_ptr, *data_ptr1;
        vxQueryImage(obj->encodeObjMutil.encode_image, VX_IMAGE_WIDTH, &width, sizeof(vx_uint32));
        vxQueryImage(obj->encodeObjMutil.encode_image, VX_IMAGE_HEIGHT, &height, sizeof(vx_uint32));

        height = 1080;

        rect.start_x = 0;
        rect.start_y = 0;
        rect.end_x = width;
        rect.end_y = height;

        status = vxMapImagePatch(obj->encodeObjMutil.encode_image,
                                 &rect,
                                 0,
                                 &map_id,
                                 &image_addr,
                                 &data_ptr,
                                 VX_WRITE_ONLY,
                                 VX_MEMORY_TYPE_HOST,
                                 VX_NOGAP_X);

        if (status == (vx_status)VX_SUCCESS)
        {
            rect.start_x = 0;
            rect.start_y = 0;
            rect.end_x = width;
            rect.end_y = height / 2;
            status = vxMapImagePatch(obj->encodeObjMutil.encode_image,
                                     &rect,
                                     1,
                                     &map_id1,
                                     &image_addr,
                                     &data_ptr1,
                                     VX_WRITE_ONLY,
                                     VX_MEMORY_TYPE_HOST,
                                     VX_NOGAP_X);
        }
        vxUnmapImagePatch(obj->encodeObjMutil.encode_image, map_id);        //释放图像ID资源
        vxUnmapImagePatch(obj->encodeObjMutil.encode_image, map_id1);       //释放图像ID资源

        //从文件内读取图像，到encodeimage内
        char filename[128] = {};
        memset(filename, 0, 128);
        sprintf(filename, "/opt/vision_apps/test_data/tivx/video_encoder/1080p_nv12_images_5num.yuv");
        FILE *fp = fopen(filename, "r");
        if (fp != NULL)
        {
            size_t ret;

            ret = fread(data_ptr, 1, width * height, fp);
            //ret = fwrite(data_ptr, 1, width * height, fp);
            if (ret != width * height)
            {
                printf("# ERROR: Unable to write data to file [%s]\n", filename);
            }
            ret = fread(data_ptr1, 1, width * height / 2, fp);
            //ret = fwrite(data_ptr1, 1, width * height / 2, fp);
            if (ret != width * height / 2)
            {
                printf("# ERROR: Unable to write data to file [%s]\n", filename);
            }

            fclose(fp);
        }
        else
        {
            printf("# ERROR: Unable to open file for writing [%s]\n", filename);
            status = VX_FAILURE;
        }


//如果使能了1080P编码，则显示部分的数据，需要进行拷贝
        status = vxCopyImagePatch(obj->displayObj.disp_image,
                                  &rect,
                                  0,                    //拷贝序号不能错误
                                  &image_addr,
                                  &data_ptr,
                                  VX_WRITE_ONLY,
                                  VX_MEMORY_TYPE_HOST);
        //第二次拷贝
        status = vxCopyImagePatch(obj->displayObj.disp_image,
                                      &rect,
                                      1,
                                      &image_addr,
                                      &data_ptr1,
                                      VX_WRITE_ONLY,
                                      VX_MEMORY_TYPE_HOST);
#endif


    if (status == VX_SUCCESS)
    {
#ifndef ENCODE_MUTIL_ENABLE     //单路编码
        APP_PRINTF("APP USB Infor : main task processGraph running…………!\n");

        uint64_t cur_time = tivxPlatformGetTimeInUsecs();

        status = vxProcessGraph(obj->graph); //执行graph

        cur_time = tivxPlatformGetTimeInUsecs() - cur_time;
        /* convert to msecs */
        cur_time = cur_time / 1000;

        APP_PRINTF("\n\n编码信息：当前帧序号:第 %d 帧 \n属性：   1080P 图像 %d x %d \n", obj->num, obj->encodeObj.inHeight, obj->encodeObj.inWidth);
        APP_PRINTF("\n\n编码信息：编码时间 = %ld ms\n", cur_time);
        obj->num++;

    //decode执行相关
    if (status == VX_SUCCESS)
    {
         if (vx_true_e == tivxIsTargetEnabled(TIVX_TARGET_VDEC1))
         {
             uint64_t cur1_time = tivxPlatformGetTimeInUsecs();
             status = app_run_decodeGraph(obj->decodeObj.graph, &obj->decodeObj, &obj->encodeObj.bitstream_obj);
             cur1_time = tivxPlatformGetTimeInUsecs() - cur1_time;
             /* convert to msecs */
             cur1_time = cur1_time / 1000;
             APP_PRINTF("\n\n解码信息：当前帧序号:第 %d 帧 \n属性：   1080P 图像 %d x %d \n", obj->num, obj->decodeObj.height, obj->decodeObj.width);
             APP_PRINTF("\n\n解码信息：解码时间 = %ld ms\n", cur1_time);
         }
    }

    if (status == VX_SUCCESS)
    {
        status = writeEncodeOutput(&obj->encodeObj);
    }

#else
        APP_PRINTF("APP Mutil Encode : processGraph running…………!\n");

        uint64_t cur_time = tivxPlatformGetTimeInUsecs();

        status = vxProcessGraph(obj->graph); //执行graph，graph内有encode节点

        cur_time = tivxPlatformGetTimeInUsecs() - cur_time;
        /* convert to msecs */
        cur_time = cur_time / 1000;

        printf("\n\n编码信息：当前帧序号:第 %d 帧 \n属性：   1080P 图像 %d x %d \n", obj->num, obj->encodeObjMutil.inHeight, obj->encodeObjMutil.inWidth);
        printf("\n\n编码信息：编码时间 = %ld ms\n", cur_time);
        obj->num++;

        uint64_t cur_time_2 = tivxPlatformGetTimeInUsecs();

        if (status == VX_SUCCESS)       //如果没问题，则将编码后的数据流，写入文件
        {
            status = writeEncodeOutput_mutil(&obj->encodeObjMutil);
        }
        cur_time_2 = tivxPlatformGetTimeInUsecs() - cur_time_2;
        /* convert to msecs */
        cur_time_2 = cur_time_2 / 1000;
        printf("\n\n数据写入时间 = %ld ms   ………………\n", cur_time_2);

#endif

    }


    if(status == VX_SUCCESS)
    {
        if ((vx_true_e == tivxIsTargetEnabled(TIVX_TARGET_DISPLAY1)) && (obj->displayObj.display_option == 1))
        {
            status = tivxMutexLock(obj->usbCameraObj.mutex); //上锁，准备获取USB数据
            if (status == VX_SUCCESS)
            {
            status = tivxMutexUnlock(obj->usbCameraObj.mutex);                                    //解锁
            }

            APP_PRINTF("app_tidl: Running display graph ... \n");
            /* Execute the display graph */
            if(status == VX_SUCCESS)
            {
                status = vxProcessGraph(obj->displayObj.disp_graph);    //执行显示的graph
            }
            APP_PRINTF("app_tidl: Running display graph ... Done.\n");
        }
    }


    return status;
}

static vx_status app_run_graph(AppObj *obj)
{
    vx_status status = VX_SUCCESS;
    vx_char curFileName[APP_MAX_FILE_PATH];
    uint64_t cur_time;
    uint64_t max_frames = INT64_MAX;
    vx_uint32 counter = 0;
    FILE* test_case_file;
    uint32_t cur_iteration;

    for(cur_iteration=0; cur_iteration<obj->num_iterations; cur_iteration++)
    {
        printf("Iteration %d of %d ... \n", cur_iteration, obj->num_iterations);

        test_case_file =  fopen(obj->input_file_list,"r");
        if(test_case_file==NULL)
        {
            break;
        }
        while (fgets(curFileName, sizeof(curFileName), test_case_file) && (counter < max_frames))
        {
            curFileName[strlen(curFileName) - 1] = 0;

            cur_time = tivxPlatformGetTimeInUsecs();


            if(status == VX_SUCCESS)
            {
                status = app_run_graph_for_one_frame(obj, curFileName, counter++);
            }


            cur_time = tivxPlatformGetTimeInUsecs() - cur_time;
            /* convert to msecs */
            cur_time = cur_time/1000;

            if(cur_time < obj->delay_in_msecs)
            {
                tivxTaskWaitMsecs(obj->delay_in_msecs - cur_time);      //系统延迟
            }

            /* user asked to stop processing */
            if(obj->stop_task || (status != VX_SUCCESS))
            {
                break;
            }
       }
       fclose(test_case_file);

       if(obj->stop_task || (status != VX_SUCCESS))
       {
           break;
       }
    }

    return status;
}


#ifdef APP_WRITE_PRE_PROC_OUTPUT
static vx_status writePreProcOutput(char* file_name, vx_tensor output)
{
    vx_status status = VX_SUCCESS;
    vx_size num_dims;
    vx_enum data_type;
    void *data_ptr;
    vx_map_id map_id;

    vx_size    start[APP_MAX_TENSOR_DIMS];
    vx_size    tensor_strides[APP_MAX_TENSOR_DIMS];
    vx_size    tensor_sizes[APP_MAX_TENSOR_DIMS];

    vxQueryTensor(output, VX_TENSOR_NUMBER_OF_DIMS, &num_dims, sizeof(vx_size));

    if(num_dims != 3)
    {
      printf("Number of dims are != 3! exiting.. \n");
      return VX_FAILURE;
    }

    vxQueryTensor(output, (vx_enum)VX_TENSOR_DIMS, tensor_sizes, 3 * sizeof(vx_size));
    vxQueryTensor(output, (vx_enum)VX_TENSOR_DATA_TYPE, &data_type, sizeof(data_type));

    start[0] = start[1] = start[2] = 0;

    tensor_strides[0] = sizeof(vx_int8);

    if((data_type == VX_TYPE_INT8) ||
       (data_type == VX_TYPE_UINT8))
    {
        tensor_strides[0] = sizeof(vx_int8);
    }
    else if((data_type == VX_TYPE_INT16) ||
            (data_type == VX_TYPE_UINT16))
    {
        tensor_strides[0] = sizeof(vx_int16);
    }
    else if((data_type == VX_TYPE_FLOAT32))
    {
        tensor_strides[0] = sizeof(vx_float32);
    }

    tensor_strides[1] = tensor_strides[0] * tensor_strides[0];
    tensor_strides[2] = tensor_strides[1] * tensor_strides[1];

    status = tivxMapTensorPatch(output, num_dims, start, tensor_sizes, &map_id, tensor_strides, &data_ptr, VX_READ_ONLY, VX_MEMORY_TYPE_HOST);

    if(VX_SUCCESS == status)
    {
      vx_char new_name[APP_MAX_FILE_PATH];
      snprintf(new_name, APP_MAX_FILE_PATH, "%s_%dx%d.rgb", file_name, (uint32_t)tensor_sizes[0], (uint32_t)tensor_sizes[1]);

      FILE *fp = fopen(new_name, "wb");
      if(NULL == fp)
      {
        printf("Unable to open file %s for writing!\n", new_name);
        return VX_FAILURE;
      }

      fwrite(data_ptr, 1, num_dims * tensor_strides[2], fp);
      fclose(fp);

      tivxUnmapTensorPatch(output, map_id);
    }

  return(status);
}
#endif
