
************************************************************************************************
#############################################Begin##############################################

时间：2021.12.31
版本V 1.0

运行方法：
1、将目录config下 cfg文件，拷贝到 开发板/opt/vision_apps/ 目录下
2、执行 ./vx_app_encode_decode_demo.out --cfg app_disp_template.cfg 即可




编译相关问题：

1、配置使能各个模块，在app_common.h 内

2、1080P 多路编码功能
打开下面两个宏定义，使能1080P 编码功能。 
#define ENCODE_MUTIL_ENABLE
#define ENCODE_DECODE_1080P_ENABLE
1080P 为多路编码。1080P 图像来自ti提供的测试数据
数据来源路径："/opt/vision_apps/test_data/tivx/video_encoder/1080p_nv12_images_5num.yuv"
多路编码生成的H 264 视频流，保存在 /home/root 路径下。

3、如果屏蔽两个宏定义，则进行720P图像的 编码、解码 工作
 A、对720P 图像进行编码
 B、保存路径 /home/root/
 C、对720P编码生成的H264视频流进行解码工作，生成YUV420 RAW data。保存路径 /home/root/

4、验证方法
 A、将生成的数据从SD 卡拷贝出来，在ubuntu下进行验证。
 
 B、scp root@192.168.1.188:/home/root/* /home/ubuntu/temp/
 进入拷贝好的图像路径执行以下命令进行验证

 C、验证YUV图像
 mplayer -demuxer rawvideo -rawvideo w=1280:h=720:format=nv12 decoder_output.yuv -fps 5

 D、验证H264视频流
 mplayer encode_output.h264

##############################################End###############################################
************************************************************************************************


