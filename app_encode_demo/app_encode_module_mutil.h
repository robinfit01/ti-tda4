/*
 *
 * Copyright (c) 2017 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef _APP_ENCODE_MODULE_MUTIL
#define _APP_ENCODE_MODULE_MUTIL

#include "app_common.h"




#define ENCODE_MUTIL_NUM       5    //进行8路编码操作

typedef struct {
  vx_node node[ENCODE_MUTIL_NUM];
  vx_kernel kernel;

  char output_file[256];
  vx_char output_file_path[128];
  FILE *out_fp[ENCODE_MUTIL_NUM];

  tivx_video_encoder_params_t encode_params;
  vx_user_data_object configuration_obj;
  vx_user_data_object bitstream_obj[ENCODE_MUTIL_NUM];
  vx_user_data_object encoded_image;

  vx_image encode_image;

  vx_int32 graph_parameter_index;
  vx_int32 inWidth;
  vx_int32 inHeight;

} EncodeObjMutil;

vx_status app_init_encode_mutil(vx_context context, EncodeObjMutil *encodeObjMutil);
void app_deinit_encode_mutil(EncodeObjMutil *encodeObjMutil);
void app_delete_encode_mutil(EncodeObjMutil *encodeObjMutil);
vx_status app_create_graph_encode_mutil(vx_graph graph, EncodeObjMutil *encodeObjMutil, vx_image *output_image);
vx_status app_running_encodeCopy_mutil(EncodeObjMutil *encodeObjMutil, char *image);
vx_status writeEncodeOutput_mutil(EncodeObjMutil *encodeObjMutil);
#endif
