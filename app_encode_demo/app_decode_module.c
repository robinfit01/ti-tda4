/*
 *
 * Copyright (c) 2017 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "app_decode_module.h"


vx_status app_init_decode(vx_context context, DecodeObj *decodeObj)
{
    vx_status status = VX_SUCCESS;

    /* Create object for encode parameters */
    tivx_video_decoder_params_init(&decodeObj->params);         //初始化参数
    decodeObj->params.bitstream_format = TIVX_BITSTREAM_FORMAT_H264; //设置解码格式为H264

    decodeObj->configuration_obj = vxCreateUserDataObject(context, 
                                                            "tivx_video_decoder_params_t", 
                                                            sizeof(tivx_video_decoder_params_t), 
                                                            NULL);

    vxCopyUserDataObject(decodeObj->configuration_obj,
                         0,
                         sizeof(tivx_video_decoder_params_t),
                         &decodeObj->params,
                         VX_WRITE_ONLY,
                         VX_MEMORY_TYPE_HOST);

    if (vxGetStatus((vx_reference)decodeObj->configuration_obj) != VX_SUCCESS)
    {
        APP_PRINTF("configuration_obj create failed\n");
        return VX_FAILURE;
    }

    decodeObj->num_buf = MAX_NUM_BUF;
    decodeObj->pipeline_depth = MAX_NUM_BUF;
    int i = 0;
    for (i = 0; i < decodeObj->num_buf; i++)
    {
        decodeObj->bitstream_obj[i] = vxCreateUserDataObject(context, "video_bitstream", decodeObj->width * decodeObj->height * 3 / 2, NULL);
        decodeObj->output_image[i] = vxCreateImage(context, decodeObj->width, decodeObj->height, VX_DF_IMAGE_NV12);        
    }

    sprintf(decodeObj->outPutfile,"/home/root/decode_graph.yuv");

    decodeObj->outPut_fp = fopen(decodeObj->outPutfile,"w+");   //打开文件

    if (NULL == decodeObj->outPut_fp)   //如果打开文件失败，则提示失效
    {
        status = VX_FAILURE;
        printf("Decode : open file %s failed!\n", decodeObj->outPutfile);
    }

    return status;
}

vx_status app_create_graph_decode(vx_graph graph, DecodeObj *decodeObj, vx_user_data_object *bitstream_obj)
{
    vx_status status = VX_SUCCESS;

    decodeObj->node = tivxVideoDecoderNode(graph,
                                           decodeObj->configuration_obj,
                                           bitstream_obj[0],
                                           decodeObj->output_image[0]);

    vxSetNodeTarget(decodeObj->node, VX_TARGET_STRING, TIVX_TARGET_VDEC1);  //设置node Target目标核

    vxSetReferenceName((vx_reference)decodeObj->node, "Decode_node");
    status = vxGetStatus((vx_reference)decodeObj->node);

    vx_graph_parameter_queue_params_t graph_parameters_queue_params_list[2];

    int graph_parameter_num = 0;

    add_graph_parameter_by_node_index(graph, decodeObj->node, 1);
    decodeObj->input_bitstream_graph_parameter_index = graph_parameter_num;
    graph_parameters_queue_params_list[graph_parameter_num].graph_parameter_index = graph_parameter_num;
    graph_parameters_queue_params_list[graph_parameter_num].refs_list_size = decodeObj->num_buf;
    graph_parameters_queue_params_list[graph_parameter_num].refs_list = (vx_reference *)&decodeObj->bitstream_obj[0];
    graph_parameter_num++;

    add_graph_parameter_by_node_index(graph, decodeObj->node, 2);
    decodeObj->output_image_graph_parameter_index = graph_parameter_num;
    graph_parameters_queue_params_list[graph_parameter_num].graph_parameter_index = graph_parameter_num;
    graph_parameters_queue_params_list[graph_parameter_num].refs_list_size = decodeObj->num_buf;
    graph_parameters_queue_params_list[graph_parameter_num].refs_list = (vx_reference *)&decodeObj->output_image[0];
    graph_parameter_num++;

    vxSetGraphScheduleConfig(graph,
                                     VX_GRAPH_SCHEDULE_MODE_QUEUE_AUTO,
                                     graph_parameter_num,
                                     graph_parameters_queue_params_list);

    tivxSetGraphPipelineDepth(graph, decodeObj->pipeline_depth);

    decodeObj->pipeline = -decodeObj->num_buf;
    decodeObj->enqueueCnt = 0;
    return status;
}

vx_status app_run_decodeGraph(vx_graph graph, DecodeObj *decodeObj, vx_user_data_object *bitStream)
{
    vx_status status = VX_SUCCESS;

    if (decodeObj->pipeline < 0)
    {
        /* Enqueue output */
        vxGraphParameterEnqueueReadyRef(graph, decodeObj->output_image_graph_parameter_index, (vx_reference *)&decodeObj->output_image[decodeObj->enqueueCnt], 1);

        app_copy_encodeBitStream_to_decodeBitStream(&decodeObj->bitstream_obj[decodeObj->enqueueCnt], bitStream);

        /* Enqueue input - start execution */
        vxGraphParameterEnqueueReadyRef(graph, decodeObj->input_bitstream_graph_parameter_index, (vx_reference *)&decodeObj->bitstream_obj[decodeObj->enqueueCnt], 1);

        decodeObj->enqueueCnt++;
        decodeObj->enqueueCnt = (decodeObj->enqueueCnt >= decodeObj->num_buf) ? 0 : decodeObj->enqueueCnt;
        decodeObj->pipeline++;
    }
    else if (decodeObj->pipeline >= 0)
    {
        vx_int32 array_idx = -1, img_array_idx = -1;
        vx_image out_image;
        vx_user_data_object in_bitstream;
        uint32_t num_refs;
        /* Dequeue & Save output */
        //参数说明：要出队的参数索引、被出队的对象填充该区域、最大的出队个数、实际的出队个数
        vxGraphParameterDequeueDoneRef(graph, decodeObj->output_image_graph_parameter_index, (vx_reference *)&out_image, 1, &num_refs);
        app_find_image_array_index(decodeObj->output_image, (vx_reference)out_image, decodeObj->num_buf, &img_array_idx);

        if (img_array_idx != -1)
        {
            app_decode_saveImageToFile("/home/root/mydecode.yuv", &decodeObj->output_image[img_array_idx]);
        }
        /* Dequeue input */
        vxGraphParameterDequeueDoneRef(graph, decodeObj->input_bitstream_graph_parameter_index, (vx_reference *)&in_bitstream, 1, &num_refs);


        /* Enqueue output */
        vxGraphParameterEnqueueReadyRef(graph, decodeObj->output_image_graph_parameter_index, (vx_reference *)&out_image, 1);
        app_find_user_object_array_index(decodeObj->bitstream_obj, (vx_reference)in_bitstream, decodeObj->num_buf, &array_idx);

        if (array_idx != -1)
        {
            app_copy_encodeBitStream_to_decodeBitStream(&decodeObj->bitstream_obj[array_idx], bitStream);
        }
        /* Enqueue input - start execution */
        vxGraphParameterEnqueueReadyRef(graph, decodeObj->input_bitstream_graph_parameter_index, (vx_reference *)&in_bitstream, 1);
     }

    return status;
}

/*
 * Utility API used to add a graph parameter from a node, node parameter index
 */
void add_graph_parameter_by_node_index(vx_graph graph, vx_node node,
                                              vx_uint32 node_parameter_index)
{
    vx_parameter parameter = vxGetParameterByIndex(node, node_parameter_index);

    vxAddParameterToGraph(graph, parameter);
    vxReleaseParameter(&parameter);
}

//拷贝encode的比特流，到decode中，等待执行
vx_status app_copy_encodeBitStream_to_decodeBitStream(vx_user_data_object *de_bitstream_obj, vx_user_data_object *en_bitstream_obj)
{
    vx_status status = VX_SUCCESS;
    vx_map_id map_id,en_map_id;
    vx_size en_bitstream_size;

    uint8_t *bitstream, *en_bitstream;

    //查询encode bitstream的流大小
    status = vxQueryUserDataObject(*en_bitstream_obj,
                                   TIVX_USER_DATA_OBJECT_VALID_SIZE,
                                   &(en_bitstream_size), sizeof(vx_size));
    APP_ASSERT(status == VX_SUCCESS);
    /* Fill the input buffer. */ //向内存映射出bitstream_obj的内存地址，大小和输入流大小相同
    status = vxMapUserDataObject(*de_bitstream_obj, 0,
                                 en_bitstream_size,
                                 &map_id, (void *)&bitstream,
                                 VX_READ_ONLY, VX_MEMORY_TYPE_HOST, 0);

    APP_ASSERT(status == VX_SUCCESS);

    status = vxMapUserDataObject(*en_bitstream_obj, 0,
                                 en_bitstream_size,
                                 &en_map_id, (void *)&en_bitstream,
                                 VX_READ_ONLY, VX_MEMORY_TYPE_HOST, 0);
    APP_ASSERT(status == VX_SUCCESS);
    APP_PRINTF("copying bitstream....................\n");
    memcpy(bitstream, en_bitstream, en_bitstream_size);
    APP_PRINTF("copying bitstream done! encodeSize = %ld\n", en_bitstream_size);

    vxUnmapUserDataObject(*de_bitstream_obj, map_id);
    vxUnmapUserDataObject(*en_bitstream_obj, en_map_id);

    tivxSetUserDataObjectAttribute(*de_bitstream_obj, TIVX_USER_DATA_OBJECT_VALID_SIZE, (void *)&en_bitstream_size, sizeof(vx_size));

    return status;
}

vx_status app_decode_saveImageToFile(char *filename , vx_image *image)
{
    vx_status status = VX_SUCCESS;
    vx_uint32 width, height;
    vx_imagepatch_addressing_t image_addr;
    vx_rectangle_t rect;
    vx_map_id map_id,map_id1;

    void *data_ptr,*data_ptr1;

    if (NULL != filename) //判断保存的路径是否为空
    {
        vxQueryImage(*image, VX_IMAGE_WIDTH, &width, sizeof(vx_uint32));
        vxQueryImage(*image, VX_IMAGE_HEIGHT, &height, sizeof(vx_uint32));

        width=1280;
        height = 720;

        rect.start_x = 0;
        rect.start_y = 0;
        rect.end_x = width;
        rect.end_y = height;

        status = vxMapImagePatch(*image,
                                 &rect,
                                 0,
                                 &map_id,
                                 &image_addr,
                                 &data_ptr,
                                 VX_WRITE_ONLY,
                                 VX_MEMORY_TYPE_HOST,
                                 VX_NOGAP_X);

        if (status == (vx_status)VX_SUCCESS)
        {
            rect.start_x = 0;
            rect.start_y = 0;
            rect.end_x = width;
            rect.end_y = height / 2;
            status = vxMapImagePatch(*image,
                                     &rect,
                                     1,
                                     &map_id1,
                                     &image_addr,
                                     &data_ptr1,
                                     VX_WRITE_ONLY,
                                     VX_MEMORY_TYPE_HOST,
                                     VX_NOGAP_X);
        }

        FILE *fp = fopen(filename, "a+");
        if (fp != NULL)
        {
            size_t ret;

            ret = fwrite(data_ptr, 1, width * height, fp);
            if (ret != width * height)
            {
                printf("# ERROR: Unable to write data to file [%s]\n", filename);
            }

            ret = fwrite(data_ptr1, 1, width * height/2, fp);
            if (ret != width * height/2)
            {
                printf("# ERROR: Unable to write data to file [%s]\n", filename);
            }

            fclose(fp);
        }
        else
        {
            printf("# ERROR: Unable to open file for writing [%s]\n", filename);
            status = VX_FAILURE;
        }
        vxUnmapImagePatch(*image, map_id);
        vxUnmapImagePatch(*image, map_id1);
    }
    else
    {
        status = VX_FAILURE;
        return status;
    }
    return status;
}

void app_find_user_object_array_index(vx_user_data_object object_array[], vx_reference ref, vx_int32 array_size, vx_int32 *array_idx)
{
    vx_int32 i;

    *array_idx = -1;
    for (i = 0; i < array_size; i++)
    {
        if (ref == (vx_reference)object_array[i])
        {
            *array_idx = i;
            break;
        }
    }
}

void app_find_image_array_index(vx_image image_array[], vx_reference ref, vx_int32 array_size, vx_int32 *array_idx)
{
    vx_int32 i;

    *array_idx = -1;
    for (i = 0; i < array_size; i++)
    {
        if (ref == (vx_reference)image_array[i])
        {
            *array_idx = i;
            break;
        }
    }
}