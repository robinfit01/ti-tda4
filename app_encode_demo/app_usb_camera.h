
#ifndef __APP_USB_CAMERA_H__
#define __APP_USB_CAMERA_H__
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <linux/videodev2.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdint.h>
#include <stdbool.h>
#include <sys/ioctl.h>
#include "app_common.h"



#define CAMERA_MUTIL //使能多摄像头功能

#ifdef CAMERA_MUTIL
#define CAMERA_NUM 1
#else
#define CAMERA_NUM 1
#endif

#define FILE_VIDEO "/dev/video1"
#define FILE_VIDEO2 "/dev/video3"

#define IMAGEWIDTH 1280
#define IMAGEHEIGHT 720

struct buffer
{
    void *start;
    unsigned int length;
};

typedef struct V4LSTRUCT
{
    //tiovx 相关
    vx_node node;
    vx_context *context;
    char *devName;
    int fd;

    struct v4l2_capability cap;
    struct v4l2_fmtdesc fmtdesc;
    struct v4l2_format fmt, fmtack;
    struct v4l2_streamparm setfps;
    struct v4l2_requestbuffers req;
    struct v4l2_buffer buf;

    struct buffer* buffers;

    enum v4l2_buf_type type;

    char *imageNV12;
    vx_image imageInputYUYV;
    vx_image imageOutputNV12;

    tivx_mutex mutex;               //创建一个任务锁，用于查询USB摄像头是否更新数据
    tivx_task task;                  //任务
    void (*run_task)(void *app_var); //函数运行指针

} USBCameraObj;

extern USBCameraObj usbCameraObj;


vx_status app_init_v4l2(USBCameraObj *usbCameraObj);
vx_status app_v4l2_grab(USBCameraObj *usbCameraObj);
vx_status app_close_v4l2(USBCameraObj *usbCameraObj);
vx_status app_init_usbCamera(vx_context *context, USBCameraObj *usbCameraObj);
vx_status app_running_usbCamera(USBCameraObj *usbCameraObj);
vx_status app_create_graph_USBColorConvert(vx_graph graph, USBCameraObj *usbCameraObj);
vx_status app_usb_camera_task_create(USBCameraObj *usbCameraObj);
void app_usb_camera_task_runVideo1(void *app_var);

void yuyvToNv12(char *image_in, char *image_out, int width, int height);
#endif
