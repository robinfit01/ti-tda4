-----------------------------------------------------------------------------------------------
运行方法：

1、此demo运行在ti 提供的SDK 0802版本上。请知悉！！！

执行 sudo make vision_apps -j16 |grep error 
进行整体编译一下。


2、将此demo整个文件夹拷贝到  ./vision_apps/apps/dl_demos/下
3、执行编译：sudo make vx_app_tidl_od_usb -j16
4、拷贝生成的文件到开发板SD 卡：
scp ./ti-processor-sdk-rtos-j721e-evm-08_00_00_12/vision_apps/out/J7/A72/LINUX/release/vx_app_tidl_od_usb.out root@192.168.1.188:/opt/vision_apps/

5、将demo目录config/app_od_usb.cfg的内容， 拷贝到开发板/opt/vision_apps/ 内

拷贝 ./models/yolov5xx_xxx/tidl_io_******.bin
	./models/yolov5xx_xxx/tidl_net_******.bin 
到SD卡的模型目录：/opt/vision_apps/test_data/psdkra/tidl_models

6、注意需要修改 cfg文件中的（有多个模型，可以分别进行测试）
	模型配置、模型网络的路径；
	如： tidl_config   /opt/vision_apps/test_data/psdkra/tidl_models/tidl_io_yolov5l6_640_1.bin
		tidl_network  /opt/vision_apps/test_data/psdkra/tidl_models/tidl_net_yolov5l6_640.bin
	
	输入模型的图像尺寸大小，
	如： dl_size   640 640 

	调整置信度大小，以识别更多的物体。
	如： viz_th    0.7  	


7、在开发板上，执行操作（一定要按照步骤执行，不然会出错！！！）

	C、./vx_app_tidl_od_usb.out --cfg app_od_usb.cfg

8、注意可能USB摄像头的设备信息在不同的板子上会不一样，这个可以查看一下 /dev/ 下的硬件列表。
	如：“video2”，这里需要修改源码中的代码
	main.c里面查找这段代码，根据自己系统的设备名称自行修改。

	sprintf(obj->usbCameraObj.devName, "/dev/video2");
	status = app_init_usbCamera(&obj->context, &(obj->usbCameraObj)); //初始化usb摄像头

9、具体操作可以参考这两篇博客：
	A、[TI TDA4 J721E]YOLOV5 模型训练结果导入及平台移植应用
		https://blog.csdn.net/AIRKernel/article/details/124250840?spm=1001.2014.3001.5501
	B、[TI TDA4 J721E]基于USB摄像头YUV数据的目标检测和Label提取（基于app_od demo)
		https://blog.csdn.net/AIRKernel/article/details/123922871?spm=1001.2014.3001.5501
-----------------------------------------------------------------------------------------------

版本更新及功能说明：
************************************************************************************************
#############################################Begin##############################################时

Version2.0
	时间：2022年04月19日
	功能：
	1、完成YOLOV5 模型各种大小的Importer转换，将onnx文件转换成TDA4平台可以识别的bin文件格式
	2、导入COCO数据集分类的Label名称
	3、实现动态USB摄像头识别物体功能

Version 1.0
	时间：2022年04月02日
	功能：
	1、实现了USB 获取YUV图像的移植。
	2、实现了USB图像动态检测目标功能
	3、实现了提取已经识别目标的标签信息，并将标签对应物体的名称。


##############################################End###############################################
************************************************************************************************
