
#ifndef _APP_COLOR_CONVERTE_MODULE
#define _APP_COLOR_CONVERTE_MODULE

#include "app_common.h"

typedef struct {
    //tiovx 相关
    vx_node node;
    vx_int32 graph_parameter_index;
    vx_image convertedImageNV12;
} ColorConvertObj;



//函数定义声明
vx_status app_init_ColorConvert(vx_context *context, ColorConvertObj *colorConvertobj);
vx_status app_create_ColorConvert(vx_graph graph,
                                  ColorConvertObj *colorConvertobj,
                                  vx_image *inImage,
                                  vx_image *outImage);

#endif
