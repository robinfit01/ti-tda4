

#include "app_usb_camera.h"

vx_status app_init_v4l2(USBCameraObj *usbCameraObj)
{
    vx_status status = VX_SUCCESS;
    //opendev
    if ((usbCameraObj->fd = open(usbCameraObj->devName, O_RDWR)) == -1)
    {
        printf("Error opening V4L interface\n");
        status = VX_FAILURE;
        return status;
    }

    //query cap
    if (ioctl(usbCameraObj->fd, VIDIOC_QUERYCAP, &usbCameraObj->cap) == -1) /* 获取设备支持的操作 */
    {
        printf("Error opening device %s: unable to query device.\n", usbCameraObj->devName);
        status = VX_FAILURE;
        return status;
    }
    else
    {
        printf("driver:\t\t%s\n", usbCameraObj->cap.driver);
        printf("card:\t\t%s\n", usbCameraObj->cap.card);
        printf("bus_info:\t%s\n", usbCameraObj->cap.bus_info);
        printf("version:\t%d\n", usbCameraObj->cap.version);
        printf("capabilities:\t%x\n", usbCameraObj->cap.capabilities);

        if ((usbCameraObj->cap.capabilities & V4L2_CAP_VIDEO_CAPTURE) == V4L2_CAP_VIDEO_CAPTURE)
        {
            printf("Device %s: supports capture.\n", usbCameraObj->devName);
        }

        if ((usbCameraObj->cap.capabilities & V4L2_CAP_STREAMING) == V4L2_CAP_STREAMING)
        {
            printf("Device %s: supports streaming.\n", usbCameraObj->devName);
        }
    }

    //emu all support fmt
    usbCameraObj->fmtdesc.index = 0;
    usbCameraObj->fmtdesc.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    printf("Support format:\n");
    while (ioctl(usbCameraObj->fd, VIDIOC_ENUM_FMT, &usbCameraObj->fmtdesc) != -1) // 列举设备所支持的格式
    {
        printf("\t%d.%s\n", usbCameraObj->fmtdesc.index + 1, usbCameraObj->fmtdesc.description);
        usbCameraObj->fmtdesc.index++;
    }

    //set fmt
    usbCameraObj->fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    usbCameraObj->fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
    usbCameraObj->fmt.fmt.pix.height = IMAGEHEIGHT;
    usbCameraObj->fmt.fmt.pix.width = IMAGEWIDTH;
    usbCameraObj->fmt.fmt.pix.field = V4L2_FIELD_INTERLACED;

    if (ioctl(usbCameraObj->fd, VIDIOC_S_FMT, &usbCameraObj->fmt) == -1) /* 设置捕获视频的格式 */
    {
        printf("Unable to set format\n");
        status = VX_FAILURE;
        return status;
    }
    if (ioctl(usbCameraObj->fd, VIDIOC_G_FMT, &usbCameraObj->fmt) == -1) /* 获取设置支持的视频格式 */
    {
        printf("Unable to get format\n");
        status = VX_FAILURE;
        return status;
    }
    

    printf("fmt.type:\t\t%d\n", usbCameraObj->fmt.type);
    printf("pix.pixelformat:\t%c%c%c%c\n", usbCameraObj->fmt.fmt.pix.pixelformat & 0xFF, (usbCameraObj->fmt.fmt.pix.pixelformat >> 8) & 0xFF, (usbCameraObj->fmt.fmt.pix.pixelformat >> 16) & 0xFF, (usbCameraObj->fmt.fmt.pix.pixelformat >> 24) & 0xFF);
    printf("pix.height:\t\t%d\n", usbCameraObj->fmt.fmt.pix.height);
    printf("pix.width:\t\t%d\n", usbCameraObj->fmt.fmt.pix.width);
    printf("pix.field:\t\t%d\n", usbCameraObj->fmt.fmt.pix.field);

    //set fps
    usbCameraObj->setfps.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    usbCameraObj->setfps.parm.capture.timeperframe.numerator = 10;
    usbCameraObj->setfps.parm.capture.timeperframe.denominator = 10;



    printf("init %s \t[OK]\n", usbCameraObj->devName);

    return status;
}

vx_status app_v4l2_grab(USBCameraObj *usbCameraObj)
{
    vx_status status = VX_SUCCESS;
    unsigned int n_buffers;

    //request for 4 buffers
    usbCameraObj->req.count = 4;
    usbCameraObj->req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    usbCameraObj->req.memory = V4L2_MEMORY_MMAP;
    if (ioctl(usbCameraObj->fd, VIDIOC_REQBUFS, &usbCameraObj->req) == -1) /* 向驱动提出申请内存的请求 */
    {
        printf("request for buffers error\n");
    }

    //mmap for buffers
    usbCameraObj->buffers = malloc(usbCameraObj->req.count * sizeof(*usbCameraObj->buffers));
    if (!usbCameraObj->buffers)
    {
        printf("Out of memory\n");
        status = VX_FAILURE;
        return status;
    }
 
    for (n_buffers = 0; n_buffers < usbCameraObj->req.count; n_buffers++)
    {
        usbCameraObj->buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        usbCameraObj->buf.memory = V4L2_MEMORY_MMAP;
        usbCameraObj->buf.index = n_buffers;
        //query buffers
        if (ioctl(usbCameraObj->fd, VIDIOC_QUERYBUF, &usbCameraObj->buf) == -1) /* 向驱动查询申请到的内存 */
        {
            printf("query buffer error\n");
            status = VX_FAILURE;
            return status;
        }

        usbCameraObj->buffers[n_buffers].length = usbCameraObj->buf.length;
        //map
        usbCameraObj->buffers[n_buffers].start = mmap(NULL, usbCameraObj->buf.length, PROT_READ | PROT_WRITE, MAP_SHARED, usbCameraObj->fd, usbCameraObj->buf.m.offset);
        if (usbCameraObj->buffers[n_buffers].start == MAP_FAILED)
        {
            printf("buffer map error\n");
            status = VX_FAILURE;
            return status;
        }
    }

    //queue
    for (n_buffers = 0; n_buffers < usbCameraObj->req.count; n_buffers++)
    {
        usbCameraObj->buf.index = n_buffers;
        ioctl(usbCameraObj->fd, VIDIOC_QBUF, &usbCameraObj->buf); /* 将空闲的内存加入可捕获视频的队列 */
    }

    usbCameraObj->type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    ioctl(usbCameraObj->fd, VIDIOC_STREAMON, &usbCameraObj->type); /* 打开视频流 */

    printf("grab yuyv OK\n");

    return status;
}

vx_status app_close_v4l2(USBCameraObj *usbCameraObj)
{
    vx_status status = VX_SUCCESS;
    ioctl(usbCameraObj->fd, VIDIOC_STREAMOFF, &usbCameraObj->cap); /* 关闭视频流 */
    if (usbCameraObj->fd != -1)
    {
        close(usbCameraObj->fd);
        return status;
    }

    status = VX_FAILURE;
    return status;
}

vx_status app_init_usbCamera(vx_context *context, USBCameraObj *usbCameraObj)
{
    vx_status status = VX_SUCCESS;

    if (status == VX_SUCCESS)
    {
        status = app_init_v4l2(usbCameraObj);

        usbCameraObj->context = context;   
        //创建两个图像
        usbCameraObj->imageOutYUYV = vxCreateImage(*context, IMAGEWIDTH, IMAGEHEIGHT, VX_DF_IMAGE_YUYV);
    }

    if (status == VX_SUCCESS)
    {
        printf("\nADDInfo:    usbCameraObj init ok!\n");
        status = app_v4l2_grab(usbCameraObj);
    }
    return status;
}

vx_status app_usb_camera_delete(USBCameraObj *usbCameraObj)
{
    vx_status status = VX_SUCCESS;
    status = app_close_v4l2(usbCameraObj);
    free(usbCameraObj->buffers);
    return status;
}



vx_status app_running_usbCamera(USBCameraObj * usbCameraObj)
{
    vx_status status = VX_SUCCESS;
    memset(&usbCameraObj->buf, 0, sizeof(usbCameraObj->buf));
    usbCameraObj->buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    usbCameraObj->buf.memory = V4L2_MEMORY_MMAP;
    ioctl(usbCameraObj->fd, VIDIOC_DQBUF, &usbCameraObj->buf); /* 将已经捕获好视频的内存拉出已捕获视频的队列 */
    ioctl(usbCameraObj->fd, VIDIOC_QBUF, &usbCameraObj->buf);  /* 将空闲的内存加入可捕获视频的队列 */

    vx_rectangle_t rect;
    vx_imagepatch_addressing_t image_addr;

    vx_uint32 img_width;
    vx_uint32 img_height;

    vxQueryImage(usbCameraObj->imageOutYUYV, VX_IMAGE_WIDTH, &img_width, sizeof(vx_uint32));
    vxQueryImage(usbCameraObj->imageOutYUYV, VX_IMAGE_HEIGHT, &img_height, sizeof(vx_uint32));

    //拷贝图像到内核空间
    rect.start_x = 0;
    rect.start_y = 0;
    rect.end_x = img_width;
    rect.end_y = img_height;

    image_addr.dim_x = img_width;
    image_addr.dim_y = img_height;
    image_addr.stride_x = 2; /* YUYV */
    image_addr.stride_y = img_width * 2;
    image_addr.scale_x = VX_SCALE_UNITY;
    image_addr.scale_y = VX_SCALE_UNITY;
    image_addr.step_x = 1;
    image_addr.step_y = 1;
    //执行拷贝操作，将USB 摄像头的YUYV图像拷贝到目标图像内
    status = vxCopyImagePatch(usbCameraObj->imageOutYUYV, //目标图像
                              &rect,
                              0,
                              &image_addr,
                              (void *)usbCameraObj->buffers[usbCameraObj->buf.index].start, // 传入需要拷贝的图像
                              VX_WRITE_ONLY,
                              VX_MEMORY_TYPE_HOST);
    return status;
}

vx_status app_usb_camera_task_create(USBCameraObj *usbCameraObj)
{
    tivx_task_create_params_t params;
    vx_status status = VX_SUCCESS;

    if (NULL != usbCameraObj->run_task) //判断函数指针是否为空
    {
        tivxTaskSetDefaultCreateParams(&params);               //初始化任务参数
        params.task_main = usbCameraObj->run_task;             //创建任务函数指针
        params.app_var = usbCameraObj;                         //传递结构体指针，给任务函数
        status = tivxTaskCreate(&usbCameraObj->task, &params); //创建任务
    }
    else
    {
        return status;
    }
    return status;
}

void app_usb_camera_task_runVideo1(void *app_var)
{
    USBCameraObj *usbCameraObj = (USBCameraObj *)app_var; //函数指针获取

    vx_status status = VX_SUCCESS;

    status = tivxMutexCreate(&usbCameraObj->mutex);
    if (VX_SUCCESS == status)
    {
        APP_PRINTF("APP USB Infor : the mutex create OK!\n");
        while (1)
        {
            if (NULL != &usbCameraObj->mutex) //如果锁为空， 则提示
            {
                APP_PRINTF(usbCameraObj->devName);
                if (status == VX_SUCCESS)
                    status = tivxMutexLock(usbCameraObj->mutex); //上锁，准备获取USB数据
                APP_PRINTF("APP USB Infor : the mutex lock ok!\n");
                if (VX_SUCCESS == status)   //查看上锁状态，如果上锁成功，则进行USB获取操作
                {
                    status = app_running_usbCamera(usbCameraObj); //usb摄像头运行

                    status = tivxMutexUnlock(usbCameraObj->mutex); //解锁
                    if (status== VX_SUCCESS)
                        APP_PRINTF("APP USB Infor : the mutex unlock ok!\n");
                }
                else
                {
                    APP_PRINTF("APP USB Infor : the mutex is busy!\n");
                }
            }
            else
            {
                APP_PRINTF("APP USB Infor : the mutex is NULL!\n");
            }
            tivxTaskWaitMsecs(10);  //必须！     
        }        
    }
    else
    {
        printf("APP USB Infor : the mutex create error!\n");
    }


}



