

const char od_labels[21][16] = {
    "background",  // label =0
    "aeroplane",   // label =1
    "bicycle",     // label =2
    "bird",        // label =3
    "boat",        // label =4
    "bottle",      // label =5
    "bus",         // label =6
    "car",         // label =7
    "cat",         // label =8
    "chair",       // label =9
    "cow",         // label =10
    "diningtable", // label =11
    "dog",         // label =12
    "horse",       // label =13
    "motorbike",   // label =14
    "person",      // label =15
    "pottedplant", // label =16
    "sheep",       // label =17
    "sofa",        // label =18
    "train",       // label =19
    "tvmonitor"    // label =20
};
