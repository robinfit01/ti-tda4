
/*************************使用方法及说明*************************/
SDK版本：0802
2022年06月29日11:02:07
使用方法：
1、将 文件夹 app_dof 文件夹替换到 ./vision_apps/apps/basic_demos 路径下
2、将 文件夹 draw2d 文件夹替换到 ./vision_apps/utils 路径下
3、执行SDK整体编译：sudo make sdk -j16 |grep error
4、执行APP整体编译：sudo make vision_apps -j16 |grep error
5、将生成的文件拷贝到SD卡中，按照app_dof的运行方式，正常运行即可。

汉字字模提取方法：
使用LCD汉字取模软件，注意需要在windows系统才能运行。
具体操作方法，比较简单，大家自行看吧。
希望对大家有所帮助！
Good Luck ！


/****************************版本变更****************************/

时间：2022年06月29日
功能：
    1、在app_dof basic_demos 基础上，显示中文字符
    2、正在移植图片显示